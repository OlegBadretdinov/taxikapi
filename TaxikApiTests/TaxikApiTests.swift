//
//  TaxikApiTests.swift
//  TaxikApiTests
//
//  Created by Oleg Badretdinov on 19.05.16.
//  Copyright © 2016 Test. All rights reserved.
//

import XCTest
import AFNetworking
import Nocilla
import Nimble
@testable import TaxikApi

private let baseUrl = NSURL(string: "http://localhost")

class TaxikApiTests: XCTestCase {
    
    var httpClient :AFHTTPSessionManager!
    var networkModel :TXNetworkModel!
    
    override func setUp() {
        super.setUp()
        LSNocilla.sharedInstance().start()
        self.httpClient = AFHTTPSessionManager(baseURL: baseUrl)
        self.networkModel = TXNetworkModel(httpClient: self.httpClient)
    }
    
    override func tearDown() {
        self.httpClient = nil
        LSNocilla.sharedInstance().stop()
        super.tearDown()
    }
    
    func testCitiesOk() {
        print(NSBundle.mainBundle().resourcePath)
        let responseString = "{\"cities\":[{\"city_id\":1,\"city_name\":\"Москва\",\"city_api_url\":\"https://beta.taxistock.ru/taxik/api/client/\",\"city_domain\":\"beta.taxistock.ru\",\"city_mobile_server\":\"protobuf.taxistock.ru:7777\",\"city_doc_url\":\"http://beta.taxistock.ru/taxik/api/doc/\",\"city_latitude\":55.75577164,\"city_longitude\":37.6177597,\"city_spn_latitude\":0.96495301,\"city_spn_longitude\":2.75756788,\"last_app_android_version\":7045,\"android_driver_apk_link\":\"http://www.taxik.ru/a/taxik.apk\",\"transfers\":true,\"registration_promocode\":true,\"experimental_econom_plus\":5,\"experimental_econom_plus_time\":40,\"inapp_pay_methods\":[\"chronopay\"],\"card_processing\":{\"id\":3,\"manual_payment\":false}},{\"city_id\":2,\"city_name\":\"Санкт-Петербург\",\"city_api_url\":\"http://piter.beta.taxistock.ru/taxik/api/client/\",\"city_domain\":\"piter.beta.taxistock.ru\",\"city_mobile_server\":\"protobuf.taxistock.ru:7788\",\"city_doc_url\":\"http://piter.beta.taxistock.ru/taxik/api/doc/\",\"city_latitude\":59.99349213,\"city_longitude\":30.2890625,\"city_spn_latitude\":0.84036398,\"city_spn_longitude\":3.01300001,\"last_app_android_version\":7045,\"android_driver_apk_link\":\"http://www.taxik.ru/a/taxik.apk\",\"transfers\":false,\"registration_promocode\":false},{\"city_id\":3,\"city_name\":\"Воронеж\",\"city_api_url\":\"http://voronezh.beta.taxistock.ru/taxik/api/client/\",\"city_domain\":\"voronezh.beta.taxistock.ru\",\"city_mobile_server\":\"protobuf.taxistock.ru:7789\",\"city_doc_url\":\"http://voronezh.beta.taxistock.ru/taxik/api/doc/\",\"city_latitude\":51.68341446,\"city_longitude\":39.18263626,\"city_spn_latitude\":0.130182,\"city_spn_longitude\":0.376625,\"last_app_android_version\":7045,\"android_driver_apk_link\":\"http://www.taxik.ru/a/taxik.apk\",\"transfers\":false,\"registration_promocode\":false},{\"city_id\":4,\"city_name\":\"Ростов-на-Дону\",\"city_api_url\":\"http://rostov.beta.taxistock.ru/taxik/api/client/\",\"city_domain\":\"rostov.beta.taxistock.ru\",\"city_mobile_server\":\"protobuf.taxistock.ru:7790\",\"city_doc_url\":\"http://rostov.beta.taxistock.ru/taxik/api/doc/\",\"city_latitude\":57.19223022,\"city_longitude\":39.41831589,\"city_spn_latitude\":0.056843,\"city_spn_longitude\":0.18831301,\"last_app_android_version\":7045,\"android_driver_apk_link\":\"http://www.taxik.ru/a/taxik.apk\",\"transfers\":false,\"registration_promocode\":false},{\"city_id\":5,\"city_name\":\"Уфа\",\"city_api_url\":\"http://ufa.beta.taxistock.ru/taxik/api/client/\",\"city_domain\":\"ufa.beta.taxistock.ru\",\"city_mobile_server\":\"protobuf.taxistock.ru:7791\",\"city_doc_url\":\"http://ufa.beta.taxistock.ru/taxik/api/doc/\",\"city_latitude\":54.73841095,\"city_longitude\":55.98345947,\"city_spn_latitude\":0.55939698,\"city_spn_longitude\":1.21673596,\"last_app_android_version\":7045,\"android_driver_apk_link\":\"http://www.taxik.ru/a/taxik.apk\",\"transfers\":false,\"registration_promocode\":false},{\"city_id\":9,\"city_name\":\"Шахты\",\"city_api_url\":\"http://rostov.beta.taxistock.ru/taxik/api/client/\",\"city_domain\":\"rostov.beta.taxistock.ru\",\"city_mobile_server\":\"protobuf.taxistock.ru:7790\",\"city_doc_url\":\"http://rostov.beta.taxistock.ru/taxik/api/doc/\",\"city_latitude\":47.70848465,\"city_longitude\":40.21595764,\"city_spn_latitude\":0.097761,\"city_spn_longitude\":0.197258,\"last_app_android_version\":7045,\"android_driver_apk_link\":\"http://www.taxik.ru/a/taxik.apk\",\"transfers\":false,\"registration_promocode\":false,\"parent_city\":4},{\"city_id\":107,\"city_name\":\"Нахабино (московская область)\",\"city_api_url\":\"http://beta.taxistock.ru/taxik/api/client/\",\"city_domain\":\"beta.taxistock.ru\",\"city_mobile_server\":\"protobuf.taxistock.ru:7777\",\"city_doc_url\":\"http://beta.taxistock.ru/taxik/api/doc/\",\"city_latitude\":55.84620285,\"city_longitude\":37.16856766,\"city_spn_latitude\":0.037595,\"city_spn_longitude\":0.043736,\"last_app_android_version\":7045,\"android_driver_apk_link\":\"http://www.taxik.ru/a/taxik.apk\",\"transfers\":false,\"registration_promocode\":false,\"parent_city\":1}]}"
        
        let dataOpt = responseString.dataUsingEncoding(NSUTF8StringEncoding)
        
        guard let data = dataOpt else {
            XCTFail("не удалось загрузить данные")
            return
        }
        
        stubRequest("GET", "http://localhost/taxik/api/client/query_cities").andReturn(200).withHeader("Content-Type", "application/json").withBody(data)
        
        var isCompleted = false
        var receivedObject :TXCitiesResponse!
        var receivedError :NSError!
        
        self.networkModel.getMapPoints().subscribeNext({ (object) in
            receivedObject = object as! TXCitiesResponse
            }, error: { (error) in
                receivedError = error
        }) {
            isCompleted = true
        }
        
        expect(isCompleted).toEventually(beTrue())
        expect(receivedObject).toNotEventually(beNil())
        expect(receivedError).toEventually(beNil())
    }
    
    func testCitiesError500() {
        stubRequest("GET", "http://localhost/taxik/api/client/query_cities").andReturn(500)
        
        var isCompleted = false
        var receivedObject :TXRootResponse!
        var receivedError :NSError!
        
        self.networkModel.getMapPoints().subscribeNext({ (object) in
            receivedObject = object as! TXRootResponse
            }, error: { (error) in
                receivedError = error
        }) {
            isCompleted = true
        }
        
        expect(isCompleted).toEventually(beFalse())
        expect(receivedObject).toEventually(beNil())
        expect(receivedError).toNotEventually(beNil())
    }
    
    func testCitiesError404() {
        stubRequest("GET", "http://localhost/taxik/api/client/query_cities").andReturn(404)
        
        var isCompleted = false
        var receivedObject :TXRootResponse!
        var receivedError :NSError!
        
        self.networkModel.getMapPoints().subscribeNext({ (object) in
            receivedObject = object as! TXRootResponse
            }, error: { (error) in
                receivedError = error
        }) {
            isCompleted = true
        }
        
        expect(isCompleted).toEventually(beFalse())
        expect(receivedObject).toEventually(beNil())
        expect(receivedError).toNotEventually(beNil())
    }
}