//
//  TXNetworkModel.swift
//  TaxikApi
//
//  Created by Oleg Badretdinov on 19.05.16.
//  Copyright © 2016 Test. All rights reserved.
//

import UIKit
import AFNetworking
import ReactiveCocoa
import Mantle

private let kMethodCities = "taxik/api/client/query_cities"

private let HTTP_GET = "GET"

public class TXNetworkModel: NSObject {

    private let httpClient :AFHTTPSessionManager
    
    public init(httpClient :AFHTTPSessionManager) {
        self.httpClient = httpClient
    }
    
    public func getMapPoints() -> RACSignal {
        return self.getSignalForMethod(HTTP_GET, path: kMethodCities)
    }
    
    private func getSignalForMethod(method :String, path :String) -> RACSignal {
        return RACSignal.createSignal({ (subscriber) -> RACDisposable! in
            return RACScheduler(priority: RACSchedulerPriorityDefault).schedule({ 
                var error :NSError? = nil
                
                let request = self.requestWithMethod(method, urlString: path, error: &error)
                
                if let requestError = error {
                    subscriber.sendError(requestError)
                    return
                }
                
                [self.startOperationWithURLRequest(request, subscriber: subscriber)]
            })
        })
    }
    
    private func requestWithMethod(method :String, urlString :String, inout error :NSError?) -> NSMutableURLRequest {
        let formatted = urlString.stringByAddingPercentEncodingWithAllowedCharacters(NSCharacterSet.URLPathAllowedCharacterSet())
        let url = NSURL(string:formatted! , relativeToURL: self.httpClient.baseURL!)
        
        let request = self.httpClient.requestSerializer.requestWithMethod(method, URLString: url!.absoluteString, parameters: nil, error: &error)
        
        return request
    }
    
    private func startOperationWithURLRequest(urlRequest :NSMutableURLRequest, subscriber :RACSubscriber) {
        let dataTask = self.httpClient.dataTaskWithRequest(urlRequest) { (response, object, error) in
            if let receivedError = error {
                subscriber.sendError(receivedError)
            } else if let receivedObject = object {
                do {
                    let rootObject = try MTLJSONAdapter.modelOfClass(TXRootResponse.self, fromJSONDictionary: receivedObject as! [NSObject : AnyObject])
                    subscriber.sendNext(rootObject)
                    subscriber.sendCompleted()
                } catch let error as NSError {
                    subscriber.sendError(error)
                }
            }
        }
        
        dataTask.resume()
    }
}
