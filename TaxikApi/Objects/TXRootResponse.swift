//
//  TXRootResponse.swift
//  TaxikApi
//
//  Created by Oleg Badretdinov on 19.05.16.
//  Copyright © 2016 Test. All rights reserved.
//

import UIKit
import Mantle

public class TXRootResponse: MTLModel, MTLJSONSerializing {
    public static func JSONKeyPathsByPropertyKey() -> [NSObject : AnyObject]! {
        return nil
    }
    
    public static func classForParsingJSONDictionary(JSONDictionary: [NSObject : AnyObject]!) -> AnyClass! {
        if JSONDictionary["cities"] != nil {
            return TXCitiesResponse.self
        }
        
        return self
    }
}
