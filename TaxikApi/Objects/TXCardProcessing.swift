//
//  TXCardProcessing.swift
//  TaxikApi
//
//  Created by Oleg Badretdinov on 20.05.16.
//  Copyright © 2016 Test. All rights reserved.
//

import UIKit
import Mantle

private let kKeyCardProcessingId = "id"
private let kKeyCardProcessingManualPayment = "manual_payment"

public class TXCardProcessing: MTLModel, MTLJSONSerializing {
    public dynamic private(set) var processingId :UInt = 0
    public dynamic private(set) var manualPayment :Bool = false
    
    public static func JSONKeyPathsByPropertyKey() -> [NSObject : AnyObject]! {
        return ["processingId" : kKeyCardProcessingId,
                "manualPayment" : kKeyCardProcessingManualPayment]
    }
}
