//
//  TXCity.swift
//  TaxikApi
//
//  Created by Oleg Badretdinov on 20.05.16.
//  Copyright © 2016 Test. All rights reserved.
//

import UIKit
import Mantle
import CoreLocation

private let kKeyCityId = "city_id"
private let kKeyCityName = "city_name"
private let kKeyCityApiUrl = "city_api_url"
private let kKeyCityDocUrl = "city_doc_url"
private let kKeyCityDomain = "city_domain"
private let kKeyCityMobileServer = "city_mobile_server"
private let kKeyCityLocationLatitude = "city_latitude"
private let kKeyCityLocationLongitude = "city_longitude"
private let kKeyCityLocationSpnLatitude = "city_spn_latitude"
private let kKeyCityLocationSpnLongitude = "city_spn_longitude"
private let kKeyCityLastAppAndroidVersion = "last_app_android_version"
private let kKeyCityAndroidDriverApkLink = "android_driver_apk_link"
private let kKeyCityTransfers = "transfers"
private let kKeyCityRegistrationPromocode = "registration_promocode"
private let kKeyCityExperimentalEconomPlus = "experimental_econom_plus"
private let kKeyCityExperimentalEconomPlusTime = "experimental_econom_plus_time"
private let kKeyCityInAppPayMethods = "inapp_pay_methods"
private let kKeyCityCardProcessing = "card_processing"

public class TXCity: MTLModel, MTLJSONSerializing {
    public private(set) var cityId :UInt = 0
    public private(set) var cityName :String!
    public private(set) var apiUrl :NSURL!
    public private(set) var docUrl :NSURL!
    public private(set) var domain :NSURL!
    public private(set) var mobileServer :NSURL!
    public private(set) var location :CLLocation!
    public private(set) var locationSpn :CLLocation!
    public private(set) var lastAppAndroidVersion :UInt = 0
    public private(set) var androidDriverApkLink :NSURL!
    public private(set) var transfers: Bool = false
    public private(set) var registrationPromocode :Bool = false
    public private(set) var experimentalEconomPlus :UInt = 0
    public private(set) var experimentalEconomPlusTime :UInt = 0
    public private(set) var inAppPayMethods :[String] = []
    public private(set) var cardProcessing :TXCardProcessing!
    
    public static func JSONKeyPathsByPropertyKey() -> [NSObject : AnyObject]! {
        return ["cityId" : kKeyCityId,
                "cityName" : kKeyCityName,
                "apiUrl" : kKeyCityApiUrl,
                "docUrl" : kKeyCityDocUrl,
                "domain" : kKeyCityDomain,
                "location" : [kKeyCityLocationLatitude, kKeyCityLocationLongitude],
                "locationSpn" : [kKeyCityLocationSpnLatitude, kKeyCityLocationSpnLongitude],
                "mobileServer" : kKeyCityMobileServer,
                "lastAppAndroidVersion" : kKeyCityLastAppAndroidVersion,
                "androidDriverApkLink" : kKeyCityAndroidDriverApkLink,
                "transfers" : kKeyCityTransfers,
                "registrationPromocode" : kKeyCityRegistrationPromocode,
                "experimentalEconomPlus" : kKeyCityExperimentalEconomPlus,
                "experimentalEconomPlusTime" : kKeyCityExperimentalEconomPlusTime,
                "inAppPayMethods" : kKeyCityInAppPayMethods,
                "cardProcessing" : kKeyCityCardProcessing]
    }
    
    public static func JSONTransformerForKey(key: String!) -> NSValueTransformer! {
        switch key {
        case "apiUrl", "docUrl", "androidDriverApkLink", "domain", "mobileServer":
            return NSValueTransformer(forName: MTLURLValueTransformerName)
            
        case "cardProcessing":
            return MTLJSONAdapter.transformerForModelPropertiesOfClass(TXCardProcessing.self)
            
        case "locationSpn":
            return MTLValueTransformer(usingForwardBlock: { (value, success, error) -> AnyObject! in
                let dict = value as! Dictionary<String, Double>
                
                if let latitude = dict[kKeyCityLocationSpnLatitude] {
                    if let longitude = dict[kKeyCityLocationSpnLongitude] {
                        success.memory = true
                        return CLLocation(latitude: latitude, longitude: longitude)
                    }
                }
                
                success.memory = false
                return nil
            })
            
        case "location":
            return MTLValueTransformer(usingForwardBlock: { (value, success, error) -> AnyObject! in
                let dict = value as! Dictionary<String, Double>
                
                if let latitude = dict[kKeyCityLocationLatitude] {
                    if let longitude = dict[kKeyCityLocationLongitude] {
                        success.memory = true
                        return CLLocation(latitude: latitude, longitude: longitude)
                    }
                }
                
                success.memory = false
                return nil
            })
            
        default:
            return nil
        }
    }
}
