//
//  TXCitiesResponse.swift
//  TaxikApi
//
//  Created by Oleg Badretdinov on 20.05.16.
//  Copyright © 2016 Test. All rights reserved.
//

import UIKit
import Mantle

private let kKeyCitiesResponseCities = "cities"

public class TXCitiesResponse: MTLModel, MTLJSONSerializing  {
    public private(set) var cities : [TXCity]!
    
    public static func JSONKeyPathsByPropertyKey() -> [NSObject : AnyObject]! {
        return ["cities" : kKeyCitiesResponseCities]
    }
    
    public static func JSONTransformerForKey(key: String!) -> NSValueTransformer! {
        if (key == "cities") {
            return MTLJSONAdapter.arrayTransformerWithModelClass(TXCity.self)
        }
        
        return nil
    }
}
